package com.java.product.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.java.product.model.ProductDetail;
import com.java.product.model.ProductDetailService;
import com.java.product.model.RequestListProductModel;
import com.java.product.service.ProductService;



@RestController
@RequestMapping("/Product")
public class ProductController {
	
	@Autowired ProductService productSv;
	
	@PostMapping("/list")
	public ResponseEntity<?> getListProduct(@RequestBody RequestListProductModel request) {
		try {
			ProductDetailService productService = productSv.getListProduct(request);
			
			return ResponseEntity.ok(ProductDetail.builder().
					productList(productService.getProductList()).
					totalPage(productService.getTotalPage()).
					build());
		}catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.internalServerError().body(e.getMessage());
		}
		

	}
	
	@GetMapping("/detail/{id}")
	public ResponseEntity<?> getProduct(@PathVariable Long id) {
		try {
			
			return ResponseEntity.ok(productSv.getProduct(id));
		}catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.internalServerError().body(e.getMessage());
		}
		
	}

}
