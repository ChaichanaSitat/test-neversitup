package com.java.product.model;

import java.util.List;

import com.java.product.entity.Product;


import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class ProductDetailService {

	private List<Product> productList;
	private Integer totalPage;
}
