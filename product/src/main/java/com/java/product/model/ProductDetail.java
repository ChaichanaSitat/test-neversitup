package com.java.product.model;

import java.util.List;

import com.java.product.entity.Product;


import lombok.*;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class ProductDetail {

	private List<Product> productList;
	private Integer totalPage;
}
