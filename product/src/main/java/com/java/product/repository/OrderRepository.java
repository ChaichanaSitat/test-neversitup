package com.java.product.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.java.product.entity.Order;
import com.java.product.entity.UserUid;



public interface OrderRepository extends JpaRepository<Order, Long>{

	public  List<Order> findByCreator(UserUid creator);
}
