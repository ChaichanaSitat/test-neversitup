package com.java.product.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.java.product.entity.Product_Order;



public interface ProductOrderRepository extends JpaRepository<Product_Order, Long>{
	

}
