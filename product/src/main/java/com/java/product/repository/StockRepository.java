package com.java.product.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.java.product.entity.Product;
import com.java.product.entity.Stock;



public interface StockRepository extends JpaRepository<Stock, Long>{

	public Optional<Stock>  findByProductId(Product productId);
}
