package com.java.product.service;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.java.product.entity.Product;
import com.java.product.model.ProductDetailService;
import com.java.product.model.RequestListProductModel;
import com.java.product.repository.ProductRepository;

@Service
public class ProductService {

	@Autowired ProductRepository productDao;
	
	public ProductDetailService getListProduct(RequestListProductModel request) {
		
		
		Pageable pageable = PageRequest.of(request.getPageNo(), request.getPageSize());
		Page<Product> productPage  = productDao.findAll(pageable);
		return ProductDetailService.builder().productList(productPage.getContent()).totalPage(productPage.getTotalPages()).build();
	}
	
	
	public Product getProduct(Long id) {
		return productDao.findById(id).get();
	}
	
}
