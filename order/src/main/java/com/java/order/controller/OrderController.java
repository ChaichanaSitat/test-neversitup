package com.java.order.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.java.order.entity.UserUid;
import com.java.order.model.CreateOrderModel;
import com.java.order.model.GenericResponse;
import com.java.order.model.OrderDetailModel;
import com.java.order.service.JwtService;
import com.java.order.service.OrderService;



@RestController
@RequestMapping("/Order")
public class OrderController {
	
	@Autowired JwtService jwtSv;
	@Autowired OrderService orderSv;

	@PostMapping("/create")
	public ResponseEntity<?> createOrder(@RequestHeader("Authorization") String Authorization ,@RequestBody CreateOrderModel request) {
		try {
			String token = Authorization.substring("Bearer".length()).trim();
			UserUid user = jwtSv.parseJWT(token);
			
			//save order
			orderSv.saveOrder(request, user);
			System.err.println("ball");
			return ResponseEntity.ok(GenericResponse.builder().code("200").massage("SUCCESS").build());
		}catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.internalServerError().body(e.getMessage());
		}
	}
	
	@GetMapping("/cancel/{id}")
	public ResponseEntity<?> cancelOrder(@RequestHeader("Authorization") String Authorization,@PathVariable Long id) {
		try {
			String token = Authorization.substring("Bearer".length()).trim();
			UserUid user = jwtSv.parseJWT(token);
			
			//cancel order
			orderSv.cancelOrder(id);
			return ResponseEntity.ok(GenericResponse.builder().code("200").massage("SUCCESS").build());
		}catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.internalServerError().body(e.getMessage());
		}
	}
	
	@GetMapping("/detail/{id}")
	public ResponseEntity<?> detailOrder(@RequestHeader("Authorization") String Authorization,@PathVariable Long id) {
		try {
			String token = Authorization.substring("Bearer".length()).trim();
			UserUid user = jwtSv.parseJWT(token);
			
			//detail order
			return ResponseEntity.ok().body(orderSv.detailOrder(id));
		}catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.internalServerError().body(e.getMessage());
		}
	}
	
}
