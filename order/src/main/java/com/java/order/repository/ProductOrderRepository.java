package com.java.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.java.order.entity.Product_Order;

public interface ProductOrderRepository extends JpaRepository<Product_Order, Long>{
	

}
