package com.java.order.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.java.order.entity.Order;
import com.java.order.entity.UserUid;

public interface OrderRepository extends JpaRepository<Order, Long>{

	public  List<Order> findByCreator(UserUid creator);
}
