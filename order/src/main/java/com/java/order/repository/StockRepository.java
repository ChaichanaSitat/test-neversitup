package com.java.order.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.java.order.entity.Product;
import com.java.order.entity.Stock;

public interface StockRepository extends JpaRepository<Stock, Long>{

	public Optional<Stock>  findByProductId(Product productId);
}
