package com.java.order.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.java.order.entity.UserUid;

public interface UserUidRepository extends JpaRepository<UserUid, Long>{

	public Optional<UserUid> findByUsername(String username);
	public Optional<UserUid> findByEmail(String email);
}
