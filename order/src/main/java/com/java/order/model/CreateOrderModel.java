package com.java.order.model;

import java.util.List;


public class CreateOrderModel {

	private List<ProductList> productList;

	public List<ProductList> getProductList() {
		return productList;
	}

	public void setProductList(List<ProductList> productList) {
		this.productList = productList;
	}
	
	
}
