package com.java.order.model;

import java.util.List;

public class OrderDetailModel {

	private List<ProductDetail> productList;

	public List<ProductDetail> getProductList() {
		return productList;
	}

	public void setProductList(List<ProductDetail> productList) {
		this.productList = productList;
	}
	
	
}
