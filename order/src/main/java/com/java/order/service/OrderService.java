package com.java.order.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.order.entity.Order;
import com.java.order.entity.Product;
import com.java.order.entity.Product_Order;
import com.java.order.entity.Stock;
import com.java.order.entity.UserUid;
import com.java.order.model.CreateOrderModel;
import com.java.order.model.OrderDetailModel;
import com.java.order.model.ProductDetail;
import com.java.order.repository.OrderRepository;
import com.java.order.repository.ProductOrderRepository;
import com.java.order.repository.ProductRepository;
import com.java.order.repository.StockRepository;
import com.java.order.util.OrderSTATUS;

@Service
public class OrderService {

	@Autowired OrderRepository orderDao;
	@Autowired ProductOrderRepository productOrderDao;
	@Autowired ProductRepository productDao;
	@Autowired StockRepository stockDao;
	
	public void saveOrder(CreateOrderModel data ,UserUid creator) {
		
		//save order frist
		Order order= orderDao.save(Order.
				builder().
				creator(creator).
				order_at(new Date()).
				status(OrderSTATUS.PENDING.name())
				.build());
		
		//save productXorder
		data.getProductList().forEach(item->{
			Product	product = productDao.findById(item.getProductId()).get();
			productOrderDao.save(Product_Order.builder()
			.productId(product)
			.orderId(order)
			.quantity(item.getQuantity())
			.price(item.getQuantity() * item.getPrice()).build()
			);
			
			
			//minus stock
			Stock stock = stockDao.findByProductId(product).get();
			stock.setQuantity(stock.getQuantity()-item.getQuantity());
			stockDao.save(stock);
			
		});
		
	
	}
	
	public void cancelOrder(Long orderId) throws Exception{
		Order order = orderDao.findById(orderId).get();
		
		if(!OrderSTATUS.PENDING.name().equalsIgnoreCase(order.getStatus())) throw new Exception("can not delete!!");
		
		order.setStatus(OrderSTATUS.DELETE.name());
		orderDao.save(order);
		
		//return to stock
		order.getProductOrder().forEach(item->{
			Stock stock = stockDao.findByProductId(item.getProductId()).get();
			stock.setQuantity(stock.getQuantity()+item.getQuantity());
			stockDao.save(stock);
		});
	}
	
	
	public OrderDetailModel detailOrder(Long orderId) {
		Order order = orderDao.findById(orderId).get();
		
		OrderDetailModel orderDetail = new OrderDetailModel();
		
		List<ProductDetail> listOrder = new ArrayList<ProductDetail>();
		order.getProductOrder().forEach(item->{
			Product product = item.getProductId();
			listOrder.add(ProductDetail
			.builder()
			.detail(product.getDetail())
			.name(product.getName())
			.reference(product.getReference())
			.quantity(item.getQuantity())
			.price(product.getPrice())
			.build()
			);
		});
		
		orderDetail.setProductList(listOrder);
		
		return orderDetail;
	}
}
