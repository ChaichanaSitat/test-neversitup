package com.java.authservice.entity;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.*;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Entity(name="userUid")
public class UserUid  implements UserDetails{

	 	/**
	 * 
	 */
	private static final long serialVersionUID = 5586713726626367018L;

		@Id
	 	@GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;
	    
	    private String username;
	    private String password;
	    private String email;
	    private Date last_login;
	    
	    

		@Override
		public Collection<? extends GrantedAuthority> getAuthorities() {
			// TODO Auto-generated method stub
			return null;
		}
		@Override
		public boolean isAccountNonExpired() {
			// TODO Auto-generated method stub
			return true;
		}
		@Override
		public boolean isAccountNonLocked() {
			// TODO Auto-generated method stub
			return true;
		}
		@Override
		public boolean isCredentialsNonExpired() {
			// TODO Auto-generated method stub
			return true;
		}
		@Override
		public boolean isEnabled() {
			// TODO Auto-generated method stub
			return true;
		}
		@Override
		public String getPassword() {
			// TODO Auto-generated method stub
			return password;
		}
		@Override
		public String getUsername() {
			// TODO Auto-generated method stub
			return username;
		}

	    


	    
	    
	    
	    
	    
	    
}

