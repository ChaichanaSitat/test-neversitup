package com.java.authservice.controller;

import java.security.Principal;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.java.authservice.config.JwtTokenUtil;
import com.java.authservice.model.GenericResponse;
import com.java.authservice.model.JwtRequest;
import com.java.authservice.model.JwtResponse;
import com.java.authservice.model.RegisterModel;
import com.java.authservice.service.JwtUserDetailsService;
import com.java.authservice.service.UserService;

@RestController
@RequestMapping("/authenticate")
public class AuthController {

//	@GetMapping("/hello")
//	public String hello(Authentication authentication, Principal principal) {
//		 System.out.println(authentication.getName());
//	        System.out.println("-----------------");
//	        System.out.println(principal.getName());
//		
//		
//		return "Hello World";
//	}
	
	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private JwtUserDetailsService jwtInMemoryUserDetailsService;

	
	@PostMapping("/login")
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) {
		try {
			authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

			UserDetails userDetails = jwtInMemoryUserDetailsService
					.loadUserByUsername(authenticationRequest.getUsername());
			final String token = jwtTokenUtil.generateToken(userDetails);
			return ResponseEntity.ok(JwtResponse.builder().jwttoken(token).build());
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.internalServerError().body(e.getMessage());
		}

		
	}

	private void authenticate(String username, String password) throws Exception {
		Objects.requireNonNull(username);
		Objects.requireNonNull(password);

		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
	
	
	@Autowired UserService userSv;
	@PostMapping("/register")
	public ResponseEntity<?> register(@RequestBody RegisterModel request) {
		try {
			
			userSv.saveUser(request);
			
			return ResponseEntity.ok(GenericResponse.builder().code("200").massage("SUCCESS").build());
		}catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.internalServerError().body(e.getMessage());
		}

	}

}
