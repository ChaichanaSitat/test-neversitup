package com.java.authservice.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.java.authservice.entity.UserUid;
import com.java.authservice.model.RegisterModel;
import com.java.authservice.repository.UserUidRepository;

@Service
public class UserService {

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired UserUidRepository userDao;
	public void saveUser(RegisterModel data) throws Exception {
		if(userDao.findByUsername(data.getUsername()).isPresent()) throw new Exception("username is already exists");
		
		String encodedPassword = bCryptPasswordEncoder.encode(data.getPassword());
		userDao.save(UserUid.builder()
				.email(data.getEmail())
				.username(data.getUsername())
				.last_login(new Date())
				.password(encodedPassword)
				.build()
				);
	}
}
