package com.java.authservice.service;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.java.authservice.entity.UserUid;
import com.java.authservice.repository.UserUidRepository;

@Service
public class JwtUserDetailsService implements UserDetailsService {

	@Autowired UserUidRepository userDao;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		Optional<UserUid> userOp = userDao.findByUsername(username);
		
		if(userOp.isPresent()) {
			return userOp.get();
		} else {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
	}

}