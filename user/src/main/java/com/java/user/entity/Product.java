package com.java.user.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;



import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Entity(name="product")
public class Product {
	
	@Id
 	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	private String detail;
	private String name;
	private String reference;
	private Double price;
	private Boolean stockable;

}
