package com.java.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.java.user.entity.UserUid;
import com.java.user.model.HistoryPageModel;
import com.java.user.model.UserProfile;
import com.java.user.service.JwtService;
import com.java.user.service.UserService;

;



@RestController
@RequestMapping("/User")
public class UserController {
	
	@Autowired JwtService jwtSv;
	@Autowired UserService userSv;

	@GetMapping("/profile")
	public ResponseEntity<?> getListProfile(@RequestHeader("Authorization") String Authorization) {
		try {
			String token = Authorization.substring("Bearer".length()).trim();
			UserUid user = jwtSv.parseJWT(token);
			
			return ResponseEntity.ok(UserProfile.builder()
					.email(user.getEmail())
					.username(user.getUsername())
					.last_login(user.getLast_login())
					.build());
		}catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.internalServerError().body(e.getMessage());
		}
	}
	
	@PostMapping("/history")
	public ResponseEntity<?> getListHistory(@RequestHeader("Authorization") String Authorization , @RequestBody HistoryPageModel request) {
		try {
			String token = Authorization.substring("Bearer".length()).trim();
			UserUid user = jwtSv.parseJWT(token);
			
			return ResponseEntity.ok(userSv.getListHistory(user,request));
		}catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.internalServerError().body(e.getMessage());
		}
	}
}
