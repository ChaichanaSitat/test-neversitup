package com.java.user.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.java.user.entity.Order;
import com.java.user.entity.Product;
import com.java.user.entity.UserUid;
import com.java.user.model.HistoryPageModel;
import com.java.user.model.OrderAt;
import com.java.user.model.OrderHistory;
import com.java.user.model.ProductDetail;
import com.java.user.repository.OrderRepository;



@Service
public class UserService {

	@Autowired OrderRepository orderDao;
	
	public OrderHistory getListHistory(UserUid creator ,HistoryPageModel request) {
		
		Pageable pageable = PageRequest.of(request.getPageNo(), request.getPageSize());
		
		OrderHistory historyList = new OrderHistory();
		List<OrderAt> orderAtList = new ArrayList<OrderAt>();
		
		Page<Order> orderPage = orderDao.findByCreator(creator,pageable);
		historyList.setTotalPage(orderPage.getTotalPages());
		
		orderPage.getContent().forEach(item->{
			OrderAt orderAt = new OrderAt();
			 List<ProductDetail> products = new ArrayList<ProductDetail>();
			try {
				orderAt.setStatus(item.getStatus());
				orderAt.setOrderAt(DateService.parseDate(item.getOrder_at()));
			} catch (ParseException e) {
				e.printStackTrace();
				return;
			}
			item.getProductOrder().forEach(action->{
				Product product = action.getProductId();
				products.add(
						ProductDetail.builder().
						detail(product.getDetail())
						.name(product.getName())
						.reference(product.getReference())
						.price(product.getPrice())
						.quantity(action.getQuantity())
						.build());
				orderAt.setProductList(products);
				
			});
			orderAtList.add(orderAt);
			
		});
		
		historyList.setListHistory(orderAtList);
		
		return historyList;
		
	}
}
