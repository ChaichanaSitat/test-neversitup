package com.java.user.service;


import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;



import org.springframework.stereotype.Service;



@Service
public class DateService {

	
	
	public static Date parseDate(String dateStr) throws ParseException {
		return new SimpleDateFormat("yyyy-MM-dd").parse(dateStr);	
	}
	
	public static String parseDate(Date date) throws ParseException {
		return new SimpleDateFormat("yyyy-MM-dd").format(date);	
	}
		
	public static String parseDateTime(Date date) throws ParseException {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(date);	
	}
	
}
