package com.java.user.model;

import java.util.List;

public class OrderAt {

	private String status;
	private String orderAt;
	private List<ProductDetail> productList;

	public List<ProductDetail> getProductList() {
		return productList;
	}

	public void setProductList(List<ProductDetail> productList) {
		this.productList = productList;
	}

	public String getOrderAt() {
		return orderAt;
	}

	public void setOrderAt(String orderAt) {
		this.orderAt = orderAt;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	
}
