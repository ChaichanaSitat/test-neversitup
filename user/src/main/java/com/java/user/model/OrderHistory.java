package com.java.user.model;

import java.util.List;

public class OrderHistory {

	private Integer totalPage;
	private List<OrderAt> listHistory;

	public List<OrderAt> getListHistory() {
		return listHistory;
	}

	public void setListHistory(List<OrderAt> listHistory) {
		this.listHistory = listHistory;
	}

	public Integer getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}
	
	
}
