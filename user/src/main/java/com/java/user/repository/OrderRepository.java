package com.java.user.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;


import com.java.user.entity.Order;
import com.java.user.entity.UserUid;





public interface OrderRepository extends JpaRepository<Order, Long> ,PagingAndSortingRepository<Order, Long>{

	public  List<Order> findByCreator(UserUid creator);
	
	public  Page<Order> findByCreator(UserUid creator ,Pageable pageable);
}
