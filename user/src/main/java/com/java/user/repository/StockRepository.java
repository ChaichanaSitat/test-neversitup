package com.java.user.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.java.user.entity.Product;
import com.java.user.entity.Stock;





public interface StockRepository extends JpaRepository<Stock, Long>{

	public Optional<Stock>  findByProductId(Product productId);
}
